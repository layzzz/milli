#Milli
##by lazy

### INSTALL
--------------

1. Drop the fonts folder to your system

2. Compile DWM
	-  Make sure you have the dependencies installed 
	-  Tailor config.mk and Makefile to your needs
	-  make && sudo make install clean
	-  That's it, dwm should be installed with the Milli theme

3. Install dunst
	- Grab the latest copy of dunst: https://github.com/knopwob/dunst
	- Modify milli.dunstrc for your screen. You can read more about
	customising dunstrc here: https://gist.github.com/fd0/9876490
	- Run dunst in the background from startup (.xinitrc, custom startup etc):
		`dunst -config milli.dunstrc &`

###USING DUNST
-----------

To interact with dunst, I use notify-send to broadcast messages.

`notify-send "Summary" "Message" -u (critical|normal|low)`

Dunst is also good for capturing messages and assigning them their own attributes. Again, read the documentation for full modification.

###Use Cases

**CMUS**

Using cmus as our music player we can broadcast whenever a track changes or is played. In the cmus commandline, hit ":"
`set status_display_program=cmus-notify.sh`
Feel free to modify the cmus-notify.sh script to your needs.

To further customise dunst to accomodate the cmus-notify.sh script, we can make an entry in the .dunstrc:

```
#...
[funny-names-here]
    summary = "Now Playing"
    background = "#FFFFFF"
    foreground = "#000000"
```

If not, the script will default to the normal behaviour of sending a normal priority message. Note that summary captures the "Now Playing" summary part of the notification. 







