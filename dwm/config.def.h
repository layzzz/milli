/* See LICENSE file for copyright and license details. */

/* appearance */

static const char font[]            = "Soleto:size=10:antialias=false:hinting=false";

static const char normbordercolor[] = "#585858"; /* #51271F */
static const char selbgcolor[]     = "#050505";
static const char selfgcolor[]     = "#FFFFFF";
static const char selbordercolor[]  = "#8DC9A6"; /* #A36D3E */
static const char normbgcolor[]      = "#050505";
static const char normfgcolor[]      = "#A2A698";

// Bonus: RETRO PURP
//static const char normbordercolor[] = "#585858"; /* #51271F */
//static const char selbgcolor[]     = "#916677";
//static const char selfgcolor[]     = "#EBDFE4";
//static const char selbordercolor[]  = "#8DC9A6"; /* #A36D3E */
//static const char normbgcolor[]      = "#916677";
//static const char normfgcolor[]      = "#AD959F";

static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int gappx     = 0;        /*was 21 -- gap pixel between windows */
static const unsigned int snap     = 1;        /*was 21 -- gap pixel between windows */
static const Bool showbar           = True;     /* False means no bar */
static const Bool topbar            = True;     /* False means bottom bar */

/* tagging */
static const char *tags[] = { "main", "www", "code", "media", "mail", "idle" };
//static const char *tags[] = { "centre", "feuilleter", "écouter", "écrire", "vue", "vide" };

static const Rule rules[] = {
    /* class      instance    title       tags mask     isfloating   monitor */
    { "Gimp",     NULL,       NULL,       0,            True,        -1 },
    { "Firefox",     NULL,       NULL,       1 << 1,            False,        -1 },
    { "Dwb",     NULL,       NULL,       0,            False,  True,        -1 },
    { "Transmission-gtk",     NULL,       NULL,       1 << 5,            False,  True,        -1 },
    // { "Deadbeef",     NULL,       NULL,       1 << 3,            False,        -1 },
    // { "Gvim",     NULL,       NULL,       1 << 2,            False,        -1 },
    // { "Subl",     NULL,       NULL,       1 << 2,            False,        -1 },
    { "Geary",     NULL,       NULL,       1 << 4,            False,        -1 },
    // { "Pcmanfm",     NULL,       NULL,       1 << 5,            False,        -1 },

};

/* layout(s) */
static const float mfact      = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster      = 1;    /* number of clients in master area */
static const Bool resizehints = False; /* True means respect size hints in tiled resizals */

#include "gaplessgrid.c"
#include "tcl.c"
#include "fibonacci.c"
static const Layout layouts[] = {
    /* symbol     arrange function */
    { "f",      NULL },    /* no layout function means floating behavior */
    { "t",      tile },    /* first entry is default */
    { "m",      monocle },
    { "fi",      spiral },
    { "dw",     dwindle },
    { "g",      gaplessgrid },
    { "d",      deck },
    { "3",      tcl },
};

/* key definitions */
//#include "push.c"
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */

static const char *dmenucmd[] = { "dmenu_run", "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, "-fn", "Soleto-10", NULL };
static const char *termcmd[]  = { "gnome-terminal", NULL, NULL, NULL, "Gnome-terminal" };
static const char *musicmcmd[]  = { "deadbeef", NULL, NULL, NULL, "Deadbeef" };
static const char *altterm[]  = { "urxvt", NULL };
static const char *scrotit[]  = { "sq.py", NULL };
static const char *qscrotit[]  = { "sq.py", "--q", NULL };
static const char *gcolor[]  = { "gcolor2", NULL, NULL, NULL, "Gcolor2" };
static const char *thunar[]    = { "pcmanfm", NULL, NULL, NULL, "Pcmanfm" }; /* fuck yeah */
static const char *browser[] = {"firefox", NULL, NULL, NULL, "Firefox"};
static const char *editor[] = {"gvim", NULL, NULL, NULL, "Gvim"};
static const char *alteditor[] = {"subl", NULL, NULL, NULL, "Sublime_text"};
static const char *musictoggle[] = { "cmus-remote", "-u", NULL };
static const char *musicshuffle[] = { "cmus-remote", "-S", NULL };
static const char *musicnext[] = { "cmus-remote", "--next", NULL };
static const char *musicback[] = { "cmus-remote", "--prev", NULL };
static const char *volumeup[] = { "pactl", "--", "set-sink-volume", "1", "+0.2", NULL };
static const char *volumedown[] = { "pactl", "--", "set-sink-volume", "1", "-0.2", NULL };
static const char *volumemute[] = { "pactl", "--", "set-sink-mute", "1", "toggle", NULL };

static Key keys[] = {
    /* modifier                     key        function        argument */
        { MODKEY,                       XK_z,     runorraise,           {.v = browser } },
        { 0,                            XK_Print,     spawn,            {.v = scrotit } },
        { ShiftMask,                    XK_Print,     spawn,            {.v = qscrotit } },
        { MODKEY,                       XK_x,     runorraise,           {.v = thunar } },
        { MODKEY,                       XK_n,      spawn,               {.v = dmenucmd } },
        { MODKEY,                       XK_c,      runorraise,          {.v = musicmcmd } },
        { MODKEY,                       XK_q,      runorraise,          {.v = gcolor } },
        { MODKEY,                       XK_e,      runorraise,          {.v = editor } },
        { MODKEY,                       XK_s,      runorraise,          {.v = alteditor } },
        { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
        // { MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
        { MODKEY,                       XK_b,      togglebar,      {0} },
        { MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
        { MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
        { MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
        { MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
        { MODKEY|ShiftMask,             XK_z,      spawn,          {.v = musicshuffle } },
        { MODKEY,                       XK_Down,   spawn,          {.v = musictoggle } },
        { MODKEY,                       XK_Right,   spawn,          {.v = musicnext } },
        { MODKEY,                       XK_Left,   spawn,          {.v = musicback } },
        { 0,                            0x1008ff12, spawn,          {.v = volumemute} },
        { 0,                            0x1008ff13, spawn,          {.v = volumeup } },
        { 0,                            0x1008ff11, spawn,          {.v = volumedown } },

        { MODKEY,                       XK_Return, zoom,           {0} },
        { MODKEY,                       XK_Tab,    view,           {0} },
        { MODKEY|ShiftMask,             XK_q,      killclient,     {0} },
        { MODKEY,                       XK_t,      setlayout,      {.v = &layouts[1]} },
        { MODKEY,                       XK_f,      setlayout,      {.v = &layouts[0]} },
        { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
        { MODKEY,                       XK_i,      swapfocus,      {0} },
        { MODKEY,                       XK_d,      setlayout,      {.v = &layouts[4]} },
        { MODKEY,                       XK_v,      setlayout,      {.v = &layouts[7]} },
        { MODKEY,                       XK_o,      setlayout,      {.v = &layouts[3]} },
        { MODKEY,                       XK_p,      setlayout,      {.v = &layouts[6]} },
        { MODKEY,                       XK_g,      setlayout,      {.v = &layouts[5]} },
        { MODKEY,                       XK_space,  setlayout,      {0} },
        { MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
        { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
        { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
        { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
        { MODKEY,                       XK_period, focusmon,       {.i = +1 } },
        { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
        { MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
    TAGKEYS(                        XK_1,                      0)
    TAGKEYS(                        XK_2,                      1)
    TAGKEYS(                        XK_3,                      2)
    TAGKEYS(                        XK_4,                      3)
    TAGKEYS(                        XK_5,                      4)
    TAGKEYS(                        XK_6,                      5)
    TAGKEYS(                        XK_7,                      6)
    TAGKEYS(                        XK_8,                      7)
    TAGKEYS(                        XK_9,                      8)
    { MODKEY|ShiftMask,             XK_x,      quit,           {0} },
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,          0,              Button2,        zoom,           {0} },
    { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

